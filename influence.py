import pyodbc
import pandas as pd
import networkx as nx
import numpy as np
import os

if os.path.isfile('total_email_data.gexf') == False:

    print ('Could not find specified graph file, time to connect to the database and make it! This might take a while.')

    cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=QLDSQL16-R04\AUSDW;DATABASE=ODS;Trusted_Connection=True')

    years = ['16', '17', '18']
    months = ['01','02','03','04','05','06','07','08','09','10','11','12']
    monthlyData  = {}

    for x, year in enumerate(years):
        for i, month in enumerate(months):
            curMonth = year + month
            print (curMonth)
            if os.path.isfile('monthdata/' + str(curMonth) + '.csv') == True:
                print ('Data already found at monthdata/' + str(curMonth) + '.csv')
                monthlyData[curMonth] = pd.read_csv('monthdata/'+ (str(curMonth)+'.csv'))
            else:
                curData = pd.read_sql("SELECT * FROM Exchange.viewInternalTracking WHERE SerialNumber LIKE ?", cnxn,
                                      params={curMonth + '%'})
                print('Month edges = ' + str(len(curData)))
                if len(curData) == 0:
                    print ('No emails found! Ignoring and moving on.')
                else:
                    curData.to_csv(path_or_buf=('monthdata/' + str(curMonth) + '.csv'))
                    print ('Files saved to monthdata/' + curMonth + '.csv')
                    monthlyData[curMonth] = pd.DataFrame(curData)

    for curGraph in monthlyData:
        G = nx.from_pandas_edgelist(monthlyData[curGraph], source ='Sender', target ='Recipient', create_using = nx.MultiDiGraph())

        eigCentral = nx.eigenvector_centrality_numpy(G)
        print ('Eig done')
        betCentral = nx.betweenness_centrality(G)
        print ('Bet done')
        degCentral = nx.degree_centrality(G)
        print ('Deg done')
        cloCentral = nx.closeness_centrality(G)
        print ('Clo done')

        betDataframe = pd.DataFrame.from_dict(betCentral, orient='index')
        degDataframe = pd.DataFrame.from_dict(degCentral, orient='index')
        cloDataframe = pd.DataFrame.from_dict(cloCentral, orient='index')
        eigDataframe = pd.DataFrame.from_dict(eigCentral, orient='index')

        betDataframe.to_csv(path_or_buf='betweenness' + str(curGraph) + '.csv')
        degDataframe.to_csv(path_or_buf='degree' + str(curGraph) + '.csv')
        cloDataframe.to_csv(path_or_buf='closeness' + str(curGraph) + '.csv')
        eigDataframe.to_csv(path_or_buf='eigenvector' + str(curGraph) + '.csv')

else:
    print ('Graph file found! Time to load it up.')
    G = nx.read_gexf('1701_1807_email_data.gexf')
    print ('The graph has ' + str(len(G)) + ' nodes')
    print ('The gprah has ' + G.size() + 'edges')

